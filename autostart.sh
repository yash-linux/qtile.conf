#!/bin/sh
nitrogen --restore & # start wallpaper engine
picom & # --experimental-backends --vsync should prevent screen tearing on most setups if needed


# Low battery notifier
~/.config/qtile/scripts/check_battery.sh & 

# Start welcome
eos-welcome & disown

/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 & # start polkit agent from GNOME
xfce4-power-manager & 
nm-applet &
volumeicon & disown

