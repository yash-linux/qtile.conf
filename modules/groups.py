from libqtile.config import Key, Group, Match
from libqtile.lazy import lazy
from .keys import keys, mod

groups = [
    Group('1', label="ҽ", matches=[Match(wm_class=["firefox", "Brave-browser"])], layout="max"),
    Group('2', label="Ȏ", matches=[Match(wm_class=["Subl", "Code"])]),
    Group('3', label="", matches=[Match(wm_class=["Xfce4-terminal", "Alacritty"])]),
    Group('4', label="參", matches=[Match(wm_class=["TelegramDesktop"])]),
    Group('5', label="ζ", matches=[Match(wm_class=["Thunar", "Pamac-manager", "ark"])]),
    Group('6', label="Ƅ", matches=[Match(wm_class=["Deadbeef"])]),
]

for i in groups:
    keys.extend([
        Key([mod], i.name, lazy.group[i.name].toscreen()),
        Key([mod], "l", lazy.screen.next_group()),
        Key([mod], "j", lazy.screen.prev_group()),
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True)),
    ])
