from libqtile import bar
from .widgets import *
from libqtile.config import Screen
import os

colors = [
    ["#282c34", "#282c34"],  # 0
    ["#1c1f24", "#1c1f24"],  # 1
    ["#dfdfdf", "#dfdfdf"],  # 2
    ["#ff6c6b", "#ff6c6b"],  # 3
    ["#98be65", "#98be65"],  # 4
    ["#da8548", "#da8548"],  # 5
    ["#5294e2", "#5294e2"],  # 6
    ["#c678dd", "#c678dd"],  # 7
    ["#46d9ff", "#46d9ff"],  # 8
    ["#a9a1e1", "#a9a1e1"],
]  # 9

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.GroupBox(
                    font="icomoon",
                    highlight_method="line",
                    block_highlight_text_color=colors[1],
                    active="#ffffff",
                    inactive="#848e96",
                    background="#2f343f",
                    highlight_color=colors[7],
                ),
                widget.Sep(),
                widget.Prompt(),
                widget.Spacer(length=5),
                widget.WindowName(
                    foreground="#99c0de", max_chars=30, font="SF Pro Display"
                ),
                
                # Right
                widget.TextBox(
                    text="賓",
                    font="icomoon",
                    fontsize="18",
                    mouse_callbacks={
                        "Button1": lambda: qtile.cmd_spawn("xfce4-terminal")
                    }
                ),
                widget.TextBox(
                    text="Ƽ",
                    font="icomoon",
                    fontsize="18",
                    mouse_callbacks={
                        "Button1": lambda: qtile.cmd_spawn("brave")
                    }
                ),
                widget.TextBox(
                    text="ү",
                    font="icomoon",
                    fontsize="18",
                    mouse_callbacks={
                        "Button1": lambda: qtile.cmd_spawn("code")
                    }
                ),
                widget.Image(filename="~/.config/qtile/modules/imgs/2.png"),
                widget.CurrentLayoutIcon(
                    scale=0.75,
                    background=colors[6],
                    foreground=colors[1],
                ),
                widget.Image(
                    filename="~/.config/qtile/modules/imgs/1.png", background=colors[6]
                ),
                widget.Net(
                    foreground=colors[1],
                    background=colors[7],
                    format="↓↑ {down}",
                    max_chars=10,
                    font="SF Pro Display",
                ),
                widget.Image(
                    filename="~/.config/qtile/modules/imgs/2.png", background=colors[7]
                ),
                widget.TextBox(
                    text="",
                    background=colors[6],
                    foreground=colors[1],
                    font="icomoon",
                    fontsize=16,
                ),
                widget.Memory(
                    format="{MemUsed: .0f}{mm}",
                    background=colors[6],
                    foreground=colors[1],
                    font="SF Pro Display",
                ),
                widget.Image(
                    filename="~/.config/qtile/modules/imgs/1.png", background=colors[6]
                ),
                widget.TextBox(
                    text="",
                    background=colors[7],
                    foreground=colors[1],
                    font="icomoon",
                    fontsize=16,
                ),
                widget.Clock(
                    format="%a %d %b, %I:%M",
                    background=colors[7],
                    foreground=colors[1],
                    font="SF Pro Display",
                ),
                widget.Image(
                    filename="~/.config/qtile/modules/imgs/2.png", background=colors[7]
                ),
                widget.Systray(icon_size=20, background=colors[6]),
            ],
            25,  # height in px
            background="#404552",  # background color
            font="Monospace",
            opacity=0.85,
        ),
    ),
]
