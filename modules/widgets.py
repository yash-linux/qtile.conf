from libqtile import widget
from libqtile import qtile

widget_defaults = dict(
    font='JetBrains Mono',
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()
